import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static swag.features.TextColors.*;

public class Task1 {
    public static void main(String[] args) throws IOException {
        System.out.println(ANSI_BOLD + "Введите x для выражения \"1 / x\".\n" + ANSI_RESET);
        func();
    }

    static void func() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            double x = Double.parseDouble(in.readLine());
            if (x == 0)
                System.out.println(1 / 0);
            else
                System.out.println(1 / x);
        } catch (ArithmeticException e) {
            System.out.println(ANSI_RED);
            System.out.println("Не дели на ноль\n");
            System.out.print(ANSI_RESET);
            func();
        } catch (NumberFormatException e) {
            System.out.println(ANSI_RED);
            System.out.println("Цифры вводи\n");
            System.out.print(ANSI_RESET);
            func();
        }
    }
}