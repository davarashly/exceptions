//Задача 3. Написать программу, которая будет считать
//        значение функции f(x) = 18*x ^ 2 + (54/x) - 8 на заданном
//        промежутке ( промежуток вводится с клавиатуры ). Далее
//        вводится с клавиатуры значение при котором нужно посчитать
//        функцию. Если значение не входит в заданный промежуток -
//        бросается исключение. Программа при этом не должна
//        завершать своей работы.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import swag.features.TextColors;
import swag.features.WrongRangeNumberException;

public class Task3 extends TextColors {
    static String fx = "18x ^ 2 + (54/x) - 8";
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static int a;
    static int b;


    Task3() throws IOException {
        System.out.println(ANSI_BOLD + "Введите левую границу промежутка для вычисления f(x) = " + fx + "." + ANSI_RESET);
        a = Integer.parseInt(in.readLine());
        System.out.println("\n" + ANSI_BOLD + "Введите правую границу промежутка для вычисления f(x) = " + fx + "." + ANSI_RESET);
        b = Integer.parseInt(in.readLine());
        if (a > b) {
            int tmp = b;
            b = a;
            a = tmp;
        }
        System.out.println("\n" + ANSI_BOLD + "Введите x для вычисления f(x) = " + fx + " на промежутке " + "[" + a + ";" + b + "]." + ANSI_RESET);
        func();
    }

    public static void main(String[] args) throws IOException {
        new Task3();
    }

    static double fx(double x) {
        return 18 * Math.pow(x, 2) + (54 / x) - 8;
    }

    static void func() throws IOException {
        try {
            double x = Double.parseDouble(in.readLine());
            if (x >= a && x <= b)
                System.out.println("\n" + ANSI_BOLD + "f(" + x + ") " + ANSI_RESET + "= " + fx(x));
            else throw new WrongRangeNumberException(a, b);
        } catch (NumberFormatException e) {
            System.out.println(ANSI_RED);
            System.out.println("Введите цифры\n");
            System.out.print(ANSI_RESET);
            func();
        } catch (WrongRangeNumberException e) {
            System.out.println(ANSI_RED);
            System.out.println(e.getMessage() + "\n");
            System.out.print(ANSI_RESET);
            func();
        }
    }
}
