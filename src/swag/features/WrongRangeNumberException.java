package swag.features;

public class WrongRangeNumberException extends Exception {
    public WrongRangeNumberException(int a, int b) {
        super("Введите число на промежутке " + "[" + a + ";" + b + "]");
    }
}