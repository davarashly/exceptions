//        Задача 2. Написать программу, которая будет считать значение функции f(x) = 12*x + 44 на
//        промежутке [-71; 14]. Значение при котором нужно посчитать значение функции вводится с
//        клавиатуры. Если оно не входит в заданный промежуток - бросается исключение. Программа при
//        этом не должна завершать своей работы.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import swag.features.TextColors;
import swag.features.WrongRangeNumberException;

public class Task2 extends TextColors {
    public static void main(String[] args) throws IOException {
        func();
    }

    static double fx(double x) {
        return 12 * x + 44;
    }

    static void func() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int a = -71, b = 14;
        try {
            System.out.println("\n" + ANSI_BOLD + "Введите x для вычисления f(x) = 12x + 44 на промежутке " + "[" + a + ";" + b + "]." + ANSI_RESET);
            double x = Double.parseDouble(in.readLine());
            if (x >= a && x <= b)
                System.out.println("\n" + ANSI_BOLD + "f(" + x + ") " + ANSI_RESET + "= " + fx(x));
            else throw new WrongRangeNumberException(a, b);
        } catch (NumberFormatException e) {
            System.out.println(ANSI_RED);
            System.out.println("Введите цифры\n");
            System.out.print(ANSI_RESET);
            func();
        } catch (WrongRangeNumberException e) {
            System.out.println(ANSI_RED);
            System.out.println(e.getMessage() + "\n");
            System.out.print(ANSI_RESET);
            func();
        }
    }
}
